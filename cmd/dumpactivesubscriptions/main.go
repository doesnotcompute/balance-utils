package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"sync"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

func mustBeNil(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	fromDB, fromDBCleanup, err := initFromDB()
	mustBeNil(err)
	defer func() {
		fromDBCleanup()
	}()
	toDB, toDBCleanup, err := initToDB()
	mustBeNil(err)
	defer func() {
		toDBCleanup()
	}()
	r := repo{
		RWMutex: &sync.RWMutex{},
		fromDB:  fromDB,
		toDB:    toDB,
	}
	mustBeNil(r.getActiveSubscribersWithBemobi(r.insertSubscriber))
}

type repo struct {
	*sync.RWMutex
	toDB   *sql.DB
	fromDB *sql.DB
}

type subscriber struct {
	msisdn      string
	renewalDays int
}

func (r repo) getActiveSubscribers(do func(s subscriber) error) error {
	handleErr := func(err error) error {
		return fmt.Errorf("get active subscribers: %w", err)
	}
	const q = "SELECT msisdn, renewal_days FROM subscriptions WHERE active = true AND ad != 'ndn'"
	rows, err := r.fromDB.Query(q)
	if err != nil {
		return handleErr(err)
	}
	defer rows.Close()
	for rows.Next() {
		var s subscriber
		if err := rows.Scan(&s.msisdn, &s.renewalDays); err != nil {
			return handleErr(err)
		}
		if err := do(s); err != nil {
			return handleErr(err)
		}
	}
	if err := rows.Err(); err != nil {
		return handleErr(err)
	}
	return nil
}

func (r repo) getActiveSubscribersWithBemobi(do func(s subscriber) error) error {
	handleErr := func(err error) error {
		return fmt.Errorf("get active subscribers: %w", err)
	}
	const q = "SELECT msisdn, renewal_days FROM subscriptions WHERE active = true"
	rows, err := r.fromDB.Query(q)
	if err != nil {
		return handleErr(err)
	}
	defer rows.Close()
	for rows.Next() {
		var s subscriber
		if err := rows.Scan(&s.msisdn, &s.renewalDays); err != nil {
			return handleErr(err)
		}
		if err := do(s); err != nil {
			return handleErr(err)
		}
	}
	if err := rows.Err(); err != nil {
		return handleErr(err)
	}
	return nil
}

func (r repo) insertSubscriber(s subscriber) error {
	handleErr := func(err error) error {
		return fmt.Errorf("insertBalance: %w", err)
	}
	defer r.lock()()
	const q = "INSERT INTO subscriber (msisdn, renewal_days) VALUES ($1, $2)"
	if _, err := r.toDB.Exec(q, s.msisdn, s.renewalDays); err != nil {
		return handleErr(err)
	}
	return nil
}

func (r repo) lock() func() {
	r.Lock()
	return func() {
		r.Unlock()
	}
}

func initFromDB() (*sql.DB, func(), error) {
	handleErr := func(err error) (*sql.DB, func(), error) {
		return nil, nil, fmt.Errorf("init from db: %w", err)
	}
	dbUser, dbPass, dbHost, dbName := os.Getenv("DB_USER"), os.Getenv("DB_PASS"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME")
	db, err := sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s", dbUser, dbPass, dbHost, dbName))
	if err != nil {
		return handleErr(err)
	}
	cleanup := func() {
		if err := db.Close(); err != nil {
			log.Print(err)
		}
	}
	if err := db.Ping(); err != nil {
		cleanup()
		return handleErr(err)
	}
	return db, cleanup, nil
}

func initToDB() (*sql.DB, func(), error) {
	handleErr := func(err error) (*sql.DB, func(), error) {
		return nil, nil, fmt.Errorf("init to db: %w", err)
	}
	db, err := sql.Open("sqlite3", "numbers.sqlite?cache=shared")
	if err != nil {
		return handleErr(err)
	}
	cleanup := func() {
		if err := db.Close(); err != nil {
			log.Print(err)
		}
	}
	if err := db.Ping(); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("PRAGMA journal_mode=WAL"); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("CREATE TABLE subscriber (id INTEGER PRIMARY KEY, msisdn TEXT NOT NULL, renewal_days INTEGER NOT NULL)"); err != nil {
		cleanup()
		return handleErr(err)
	}
	return db, cleanup, nil
}
