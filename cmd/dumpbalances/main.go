package main

import (
	"bufio"
	"compress/gzip"
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	_ "github.com/mattn/go-sqlite3"
)

func mustBeNil(err error) {
	if err != nil {
		panic(err)
	}
}

const dirName = "logs"

func main() {
	logs, err := ioutil.ReadDir(dirName)
	mustBeNil(err)
	db, cleanup, err := initDB()
	mustBeNil(err)
	defer cleanup()
	r := repo{db: db, RWMutex: &sync.RWMutex{}}
	wg := sync.WaitGroup{}
	wg.Add(len(logs))
	for _, fi := range logs {
		fi := fi
		go func() {
			defer func() {
				wg.Done()
			}()
			filename := filepath.Join(dirName, fi.Name())
			log.Printf("started processing %s", filename)
			mustBeNil(r.processFile(filename))
			log.Printf("finished processing %s", filename)
		}()
	}
	wg.Wait()
}

func (r repo) processGzipFile(filename string) error {
	handleErr := func(err error) error {
		return fmt.Errorf("processFile: %w", err)
	}
	f, err := os.Open(filename)
	if err != nil {
		return handleErr(err)
	}
	defer f.Close()
	gr, err := gzip.NewReader(f)
	if err != nil {
		return handleErr(err)
	}
	defer gr.Close()
	s := bufio.NewScanner(gr)
	for s.Scan() {
		line := strings.Split(s.Text(), ",")
		if len(line) != 2 {
			return handleErr(fmt.Errorf("line length != 2: %v", line))
		}
		msisdn, balStr := line[0], line[1]
		bal, err := strconv.Atoi(balStr)
		if err != nil {
			return handleErr(err)
		}
		if err := r.insertBalance(msisdn, bal, filename); err != nil {
			return handleErr(err)
		}
	}
	if err := s.Err(); err != nil {
		return handleErr(err)
	}
	return nil
}

func (r repo) processFile(filename string) error {
	handleErr := func(err error) error {
		return fmt.Errorf("processFile: %w", err)
	}
	f, err := os.Open(filename)
	if err != nil {
		return handleErr(err)
	}
	defer f.Close()
	s := bufio.NewScanner(f)
	for s.Scan() {
		line := strings.Split(s.Text(), ",")
		if len(line) != 2 {
			return handleErr(fmt.Errorf("line length != 2: %v", line))
		}
		msisdn, balStr := line[0], line[1]
		bal, err := strconv.Atoi(balStr)
		if err != nil {
			return handleErr(err)
		}
		if err := r.insertBalance(msisdn, bal, filename); err != nil {
			return handleErr(err)
		}
	}
	if err := s.Err(); err != nil {
		return handleErr(err)
	}
	return nil
}

func initDB() (*sql.DB, func(), error) {
	handleErr := func(err error) (*sql.DB, func(), error) {
		return nil, nil, fmt.Errorf("init db: %w", err)
	}
	db, err := sql.Open("sqlite3", "numbers.sqlite?cache=shared")
	if err != nil {
		return handleErr(err)
	}
	cleanup := func() {
		if err := db.Close(); err != nil {
			log.Print(err)
		}
	}
	if err := db.Ping(); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("PRAGMA journal_mode=WAL"); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("CREATE TABLE IF NOT EXISTS balance (id INTEGER PRIMARY KEY, msisdn TEXT NOT NULL, amount INTEGER NOT NULL, date TEXT NOT NULL)"); err != nil {
		cleanup()
		return handleErr(err)
	}
	return db, cleanup, nil
}

type repo struct {
	*sync.RWMutex
	db *sql.DB
}

func (r repo) lock() func() {
	r.Lock()
	return func() {
		r.Unlock()
	}
}

func (r repo) insertBalance(msisdn string, balance int, date string) error {
	handleErr := func(err error) error {
		return fmt.Errorf("insertBalance: %w", err)
	}
	defer r.lock()()
	const q = "INSERT INTO balance (msisdn, amount, date) VALUES ($1, $2, $3)"
	if _, err := r.db.Exec(q, msisdn, balance, date); err != nil {
		return handleErr(err)
	}
	return nil
}

func (r repo) getMaxBalance(msisdn string) (int, error) {
	handleErr := func(err error) (int, error) {
		return 0, fmt.Errorf("getMaxBalance: %w", err)
	}
	const q = "SELECT MAX(amount) FROM balance WHERE msisdn = $1"
	var amount int
	if err := r.db.QueryRow(q, msisdn).Scan(&amount); err != nil {
		return handleErr(err)
	}
	return amount, nil
}
