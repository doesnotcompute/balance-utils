package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

const minBalance = 478

func main() {
	// get dirname from args
	if len(os.Args) != 3 {
		fmt.Printf("%s outputs a list of msisdns eligible for purging according to the specified params.\n", filepath.Base(os.Args[0]))
		fmt.Printf("Usage: %s <dirname> <local db name>\n", filepath.Base(os.Args[0]))
		os.Exit(1)
	}
	dirName := os.Args[1]
	localDBName := os.Args[2]
	localDB, cleanup, err := initLocalDB(localDBName)
	mustBeNil(err)
	defer cleanup()
	dbUser, dbPass, dbHost, dbName := os.Getenv("DB_USER"), os.Getenv("DB_PASS"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME")
	pg, cleanupPG, err := initPG(dbUser, dbPass, dbHost, dbName)
	mustBeNil(err)
	defer cleanupPG()
	r := repo{localDB: localDB, remoteDB: pg, RWMutex: &sync.RWMutex{}}

	// save logs in the local db
	mustBeNil(readLogs(r, dirName))
	// save active subscribers in the local db
	mustBeNil(r.getActiveSubscribers(r.insertSubscriber))
	// get purge candidates
	mustBeNil(r.getPurgeCandidates(minBalance, func(msisdn string) error {
		if _, err := os.Stdout.WriteString(msisdn + "\n"); err != nil {
			return err
		}
		return nil
	}))
}

func mustBeNil(err error) {
	if err != nil {
		panic(err)
	}
}

func readLogs(r repo, dirName string) error {
	handleErr := func(err error) error {
		return fmt.Errorf("read logs from %s: %w", dirName, err)
	}
	logs, err := ioutil.ReadDir(dirName)
	if err != nil {
		return handleErr(err)
	}
	wg := sync.WaitGroup{}
	wg.Add(len(logs))
	for _, fi := range logs {
		fi := fi
		go func() {
			defer func() {
				wg.Done()
			}()
			filename := filepath.Join(dirName, fi.Name())
			log.Printf("started processing %s", filename)
			if err := r.processFile(filename); err != nil {
				log.Print(err)
				return
			}
			log.Printf("finished processing %s", filename)
		}()
	}
	wg.Wait()
	return nil
}

func (r repo) processFile(filename string) error {
	handleErr := func(err error) error {
		return fmt.Errorf("processFile: %w", err)
	}
	f, err := os.Open(filename)
	if err != nil {
		return handleErr(err)
	}
	defer f.Close()
	s := bufio.NewScanner(f)
	for s.Scan() {
		line := strings.Split(s.Text(), ",")
		if len(line) != 2 {
			return handleErr(fmt.Errorf("line length != 2: %v", line))
		}
		msisdn, balStr := line[0], line[1]
		bal, err := strconv.Atoi(balStr)
		if err != nil {
			return handleErr(err)
		}
		if err := r.insertBalance(msisdn, bal, filename); err != nil {
			return handleErr(err)
		}
	}
	if err := s.Err(); err != nil {
		return handleErr(err)
	}
	return nil
}

func initLocalDB(fileName string) (*sql.DB, func(), error) {
	handleErr := func(err error) (*sql.DB, func(), error) {
		return nil, nil, fmt.Errorf("init db: %w", err)
	}
	db, err := sql.Open("sqlite3", fmt.Sprintf("%s?cache=shared", fileName))
	if err != nil {
		return handleErr(err)
	}
	cleanup := func() {
		if err := db.Close(); err != nil {
			log.Print(err)
		}
	}
	if err := db.Ping(); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("PRAGMA journal_mode=WAL"); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("CREATE TABLE balance (id INTEGER PRIMARY KEY, msisdn TEXT NOT NULL, amount INTEGER NOT NULL, date TEXT NOT NULL)"); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("CREATE TABLE subscriber (id INTEGER PRIMARY KEY, msisdn TEXT NOT NULL, renewal_days INTEGER NOT NULL)"); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("CREATE INDEX msisdn_b_idx ON balance (msisdn)"); err != nil {
		cleanup()
		return handleErr(err)
	}
	if _, err := db.Exec("CREATE INDEX msisdn_s_idx ON subscriber (msisdn)"); err != nil {
		cleanup()
		return handleErr(err)
	}
	return db, cleanup, nil
}

type repo struct {
	*sync.RWMutex
	localDB  *sql.DB
	remoteDB *sql.DB
}

func (r repo) lock() func() {
	r.Lock()
	return func() {
		r.Unlock()
	}
}

func (r repo) insertBalance(msisdn string, balance int, date string) error {
	handleErr := func(err error) error {
		return fmt.Errorf("insertBalance: %w", err)
	}
	defer r.lock()()
	const q = "INSERT INTO balance (msisdn, amount, date) VALUES ($1, $2, $3)"
	if _, err := r.localDB.Exec(q, msisdn, balance, date); err != nil {
		return handleErr(err)
	}
	return nil
}

type subscriber struct {
	msisdn      string
	renewalDays int
}

func (r repo) insertSubscriber(s subscriber) error {
	handleErr := func(err error) error {
		return fmt.Errorf("insertBalance: %w", err)
	}
	defer r.lock()()
	const q = "INSERT INTO subscriber (msisdn, renewal_days) VALUES ($1, $2)"
	if _, err := r.localDB.Exec(q, s.msisdn, s.renewalDays); err != nil {
		return handleErr(err)
	}
	return nil
}

func (r repo) getPurgeCandidates(minBalance int, do func(msisdn string) error) error {
	handleErr := func(err error) error {
		return fmt.Errorf("getPurgeCandidates: %w", err)
	}
	defer r.lock()()
	const q = "SELECT b.msisdn FROM balance b JOIN subscriber s ON b.msisdn = s.msisdn GROUP BY b.msisdn HAVING MAX(b.amount) < $1"
	rows, err := r.localDB.Query(q, minBalance)
	if err != nil {
		return handleErr(err)
	}
	for rows.Next() {
		var msisdn string
		if err := rows.Scan(&msisdn); err != nil {
			return handleErr(err)
		}
		if err := do(msisdn); err != nil {
			return handleErr(err)
		}
	}
	defer rows.Close()
	if err := rows.Err(); err != nil {
		return handleErr(err)
	}
	return nil
}

func (r repo) getActiveSubscribers(do func(s subscriber) error) error {
	handleErr := func(err error) error {
		return fmt.Errorf("get active subscribers: %w", err)
	}
	const q = "SELECT msisdn, renewal_days FROM subscriptions WHERE active = true AND ad != 'ndn'"
	rows, err := r.remoteDB.Query(q)
	if err != nil {
		return handleErr(err)
	}
	defer rows.Close()
	for rows.Next() {
		var s subscriber
		if err := rows.Scan(&s.msisdn, &s.renewalDays); err != nil {
			return handleErr(err)
		}
		if err := do(s); err != nil {
			return handleErr(err)
		}
	}
	if err := rows.Err(); err != nil {
		return handleErr(err)
	}
	return nil
}

func initPG(dbUser, dbPass, dbHost, dbName string) (*sql.DB, func(), error) {
	handleErr := func(err error) (*sql.DB, func(), error) {
		return nil, nil, fmt.Errorf("init from db: %w", err)
	}
	db, err := sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s", dbUser, dbPass, dbHost, dbName))
	if err != nil {
		return handleErr(err)
	}
	cleanup := func() {
		if err := db.Close(); err != nil {
			log.Print(err)
		}
	}
	if err := db.Ping(); err != nil {
		cleanup()
		return handleErr(err)
	}
	return db, cleanup, nil
}
