# Jazz msisdn purger

## Usage

1. Fetch the balance logs to be analyzed from the `xmp-jazz-balance-log` s3 bucket into a local directory (e.g. `logs`)

Example:
```
aws s3 cp --recursive --exclude "*" --include "202106*.log" s3://xmp-jazz-balance-log/ .
```

The above example shows how to copy balance logs for June 2021.

2. Run the executable and specify the following params:

- `<dirname>` - the name of the directory containing the necessary balance logs
- `<local db name>` - file name for the local sqlite database

Example:
```
./getnumbers balance-logs workfile.sqlite > outfile.txt

```

The above command will write all the numbers eligible for purging into `outfile.txt`.

Note: the command may run for several hours, depending on the number and size of log files to analyze.

3. To mark certain numbers as purged, execute the following sql query in the jazz prod database:
```
UPDATE subscriptions SET purged = true, active = false, updated_at = now() WHERE msisdn IN (...);
```
replacing the `...` with a list of msisdns to purge.
