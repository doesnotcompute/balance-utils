module bl

go 1.15

require (
	github.com/lib/pq v1.9.0
	github.com/mattn/go-sqlite3 v1.14.5
)
